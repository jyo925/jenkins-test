package com.example.demo;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class PersonController {

    @GetMapping("/")
    public void test(){
        log.info("I am log 1");
        log.info("I am log 2");
        log.info("I am log 3");

    }

    @GetMapping("/myinfo")
    public Person getPerson() {
        Person person = new Person();
        person.setName("HDH");
        person.setAge(25);
        person.setAddress("경기도");
        person.setJob("대학생");
        log.info("I am log 1");
        log.info("I am log 2");
        log.info("I am log 3");
        return person;
    }
}